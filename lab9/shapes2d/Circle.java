package shapes2d;

public class Circle {
    protected final int radius;

    public Circle(int radius) {
        this.radius = radius;
    }

    public double area() {
        return Math.PI * radius * radius;
    }

    @Override
    public String toString() {
        return "radius = " + radius;
    }

    @Override
    public boolean equals(Object obj) {
        if ( this == obj)
            return true;
        if (obj instanceof Circle) { //if I am really circle object
            Circle c = (Circle) obj;   // this is down casting
            return radius == c.radius;
        }
        return false;

    }
}
