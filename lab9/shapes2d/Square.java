package shapes2d;

public class Square {
    protected final int side;

    public Square(int side) {
        this.side = side;
    }

    public int area() {
        return side * side;
    }

    public String toString() {
        return "side = " + side;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj.getClass().equals(this.getClass())) { //if I am really circle object
            Square c = (Square) obj;   // this is down casting
            return side == c.side;
        }
        return false;
    }
}
