package shapes2d;

public class TestShapes2D {
    public static void main(String[] args){
        Circle c1 = new Circle(5);
        c1 = new Circle(6); // I am not updating this, I am just creating another one
        Circle c2 = new Circle(6);
        String str = new String("Hello"); //immutable
        String str2 = "Hola";
    // str == str2 never do this ! because it compares object not content
        System.out.println(str.equals(str2));  // Correct comparison


        System.out.println("c1 == c2 : " + (c2 == c1));

        System.out.println("c1.equals(c2) : " + c1.equals(c2) );


    }
}
